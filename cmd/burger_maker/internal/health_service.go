package internal

import (
	"encoding/json"
	"net/http"
	"strings"
)

type HealthResponse struct {
	Status string `json:"status"`
}

/**
 * healthcheck endpoint
 */
func Health(writer http.ResponseWriter, request *http.Request) {
	if strings.ToUpper(request.Method) != "GET" {
		http.Error(writer, "Method not allowed", http.StatusMethodNotAllowed)
	} else {
		body, err := json.Marshal(HealthResponse{Status: "OK"})
		if err != nil {
			http.Error(writer, err.Error(), http.StatusInternalServerError)
			return
		}
		writer.Header().Set("Content-Type", "application/json")
		writer.WriteHeader(http.StatusOK)
		if _, err = writer.Write(body); err != nil {
			panic("Error while sending health status response")
		}
	}
}
