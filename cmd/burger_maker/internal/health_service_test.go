package internal

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

const port = 8080

func Test_healthcheck_ok(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/health", port)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	Health(res, req)

	if res.Code != http.StatusOK {
		t.Fatalf("Response code was %v - %s", res.Code, res.Body.String())
	}
	var healthResponse HealthResponse
	expectedErrorStatus := "OK"
	if err := json.Unmarshal(res.Body.Bytes(), &healthResponse); err != nil {
		t.Fatal(err)
	} else if healthResponse.Status != expectedErrorStatus {
		t.Fatalf("Invalid error status. Expected %s, got %s", expectedErrorStatus, healthResponse.Status)
	}
}

func Test_method_not_allowed(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/health", port)
	methods := []string{"POST", "PATCH", "HEAD", "DELETE"}
	for _, method := range methods {
		req, err := http.NewRequest(method, url, nil)
		if err != nil {
			t.Fatal(err)
		}
		res := httptest.NewRecorder()

		Health(res, req)
		if res.Code != http.StatusMethodNotAllowed {
			t.Fatalf("Response code was %v for method %s and should be %d", res.Code, method, http.StatusMethodNotAllowed)
		}
	}
}
