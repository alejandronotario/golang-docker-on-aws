package internal

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func Test_menu_should_be_as_expected(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/api/recipes", port)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	GetRecipes(res, req)

	if res.Code != http.StatusOK {
		t.Fatalf("Response code was %v - %s", res.Code, res.Body.String())
	}
	var recipes []string
	if err := json.Unmarshal(res.Body.Bytes(), &recipes); err != nil {
		t.Fatal(err)
	} else if len(recipes) != 6 {
		t.Fatalf("Unexpected number of recipes. Expected %d, got %s", 6, recipes)
	}
}

func Test_cheeseburger_should_be_as_expected(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/api/burgers/cheeseburger", port)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	GimeABurger(res, req)

	if res.Code != http.StatusOK {
		t.Fatalf("Response code was %v - %s", res.Code, res.Body.String())
	}
	var burger Burger
	if err := json.Unmarshal(res.Body.Bytes(), &burger); err != nil {
		t.Fatal(err)
	} else {
		if burger.Name != "cheeseburger" {
			t.Fatalf("Unexpected burger recipe. Expected %s, got %s", "cheeseburger", burger.Name)
		}
		if len(burger.Ingredients) != 7 {
			t.Fatalf("Unexpected number of ingredient. Expected %d, got %d", 7, len(burger.Ingredients))
		}
		if burger.Ingredients[0].Name != "bun" {
			t.Fatalf("Unexpected ingredient 0. Expected %s, got %s", "bun", burger.Ingredients[0].Name)
		}
		if burger.Ingredients[1].Name != "red onion" {
			t.Fatalf("Unexpected ingredient 1. Expected %s, got %s", "red onion", burger.Ingredients[1].Name)
		}
		if burger.Ingredients[2].Name != "tomato" {
			t.Fatalf("Unexpected ingredient 2. Expected %s, got %s", "tomato", burger.Ingredients[2].Name)
		}
		if burger.Ingredients[3].Name != "ketchup" {
			t.Fatalf("Unexpected ingredient 3. Expected %s, got %s", "ketchup", burger.Ingredients[3].Name)
		}
		if burger.Ingredients[4].Name != "salad" {
			t.Fatalf("Unexpected ingredient 4. Expected %s, got %s", "salad", burger.Ingredients[4].Name)
		}
		if burger.Ingredients[5].Name != "cheddar" {
			t.Fatalf("Unexpected ingredient 5. Expected %s, got %s", "cheddar", burger.Ingredients[5].Name)
		}
		if burger.Ingredients[6].Name != "steak" {
			t.Fatalf("Unexpected ingredient 6. Expected %s, got %s", "steak", burger.Ingredients[6].Name)
		}
	}
}

func Test_anyburger_should_be_delivered(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/api/burgers/any", port)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	GimeABurger(res, req)

	if res.Code != http.StatusOK {
		t.Fatalf("Response code was %v - %s", res.Code, res.Body.String())
	}
	var burger Burger
	if err := json.Unmarshal(res.Body.Bytes(), &burger); err != nil {
		t.Fatal(err)
	}
}

func Test_nonexistingburger_should_fail(t *testing.T) {
	url := fmt.Sprintf("http://localhost:%d/api/burgers/nosuchrecipe", port)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatal(err)
	}

	res := httptest.NewRecorder()

	GimeABurger(res, req)

	if res.Code != http.StatusNotFound {
		t.Fatalf("Response code was %v - %s", res.Code, res.Body.String())
	}
}
