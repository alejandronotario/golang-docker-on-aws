module to-be-continuous/burger-maker

go 1.13

require (
	github.com/google/uuid v1.3.0
	golang.org/x/tools/gopls v0.6.9 // indirect
	google.golang.org/grpc v1.47.0
)
