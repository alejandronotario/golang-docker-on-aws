FROM busybox:1.35.0 AS busybox

FROM scratch

ARG CI_PROJECT_URL
ENV PORT=80

# hadolint ignore=DL3048
LABEL Name="burger-maker" \
      Version="1.0.0" \
      Maintainer="tbc-dev@googlegroups.com" \
      Description="Burger Maker sample application (developed in Go)" \
      Url=${CI_PROJECT_URL}

COPY --from=busybox /bin/wget   /wget

COPY static       /static
COPY bin/burger_maker       /burger_maker

HEALTHCHECK --interval=30s --timeout=5s \
    CMD ["/wget", "-Y", "off", "-O", "-", "http://localhost/health" ]

EXPOSE  ${PORT}
CMD ["/burger_maker"]
